<?php
/**
 * @package WordPress
 * @subpackage gulpstarter
 */
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- SEO Meta --><!-- SEO Meta -->
    <meta name="referrer" content="always" />
    <meta name="keywords" content="custom apparel, custom t-shirts, custom designed shirts" />
    <meta name="geo.region" content="Texas" />
    <meta name="geo.position" content="32.9819866,-96.8471715" />
    <meta name="geo.placename" content="In Your Face Apparel" />
    <title><?php wp_title(); ?></title>

    <!-- Minified CSS -->
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/dist/css/main.css">

    <!-- Wordpress Injected Code -->
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>