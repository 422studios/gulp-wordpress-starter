<?php
/**
 * @package WordPress
 * @subpackage gulpstarter
 */
?>

		<!-- Wordpress Injected Javascript -->
		<?php wp_footer(); ?>

		<!-- Javscript Files -->
    	<input type="hidden" id="siteUrl" value="<?php echo site_url(); ?>/">
    	<script src="<?php echo get_bloginfo('template_url'); ?>/assets/dist/js/app.min.js"></script>
	</body>
</html>
