<?php

add_theme_support( 'post-thumbnails' );
add_theme_support('woocommerce');


function arphabet_widgets_init() {

	//Add sidebars

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


//Add menus
function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

//Add ACF Options Page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page('Theme Settings');

}

//Hide Admin Bar
show_admin_bar( false );

/* Register Custom Navigation Walker For Bootstrap Dropdowns
require_once('wp_bootstrap_navwalker.php');
*/

/* Change Excerpt length */
function custom_excerpt_length( $length ) { return 30; }
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


//Enable SVG Uploads
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
