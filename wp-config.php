<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iyfapparel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W8=tx*la-Ux=eY.`8,<AYvk)g]n^TKux4=C>A+9cYyH#*>)0Oy3f-TrOL=B4NzX&');
define('SECURE_AUTH_KEY',  '`4z5Q]?gYGborBx#rrhx[a?{CiO9Y+*E-<Ng8 9[Ti1S/kjfI)qM,)9AYwVsa<#)');
define('LOGGED_IN_KEY',    ';-ND]rtO2J$cEyK_-c%Mf]8B+5m!R:~S.%JFh|.6/hx~;} iCh9v;]|?>YAyrIHw');
define('NONCE_KEY',        'MFb:(.qtkrWB mKu&l4Zmhe;jO7*wA*F!B*/+?0p>(i8MOiwAgyLzn?8&&NVixUS');
define('AUTH_SALT',        '8_bo`]Z8/!b9LjV%P:]</$]T*W4y;4|W22~bc:jCEq16.Cn2&AOzM^B.A*9wvfZU');
define('SECURE_AUTH_SALT', 'W2bUf-3tPk%WN6AHL+3z&eEA@a`Vo?N>j:}-J},&B7JI^N`MGvg=t%ZED>2;Yfih');
define('LOGGED_IN_SALT',   'M8~b6EtTzFH:2}`8rbVj,`Ote|>c,h7iSuU#>-L9hH|:a,8V^9B``7D0d8rR(!4{');
define('NONCE_SALT',       'Jt>5_r5hG;ui^g9]RLkDs+q8xW3`?<g7j8 kxE}@4#=JWvsBJQU6ss<?HUm+ZA6c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
